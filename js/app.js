$(function() {
    $.ajaxSetup({
        beforeSend: function(xhr) {
            var token = sessionStorage.getItem("token");
            xhr.setRequestHeader("Authorization", "Bearer " + token);
        },
    });

    $(document).ajaxStart(function() {
        $("#spinner").show();
    });

    $(document).ajaxStop(function() {
        setTimeout(function() {
            $("#spinner").hide();
        }, 500);
    });

    function parseHash(newHash, oldHash) {
        crossroads.parse(newHash);
    }

    // general function start
    function checkUserRole(permissionFor) {
        var token = sessionStorage.getItem("token");
        //decode jwt token using jwt-decode
        var decoded = jwt_decode(token);
        var currentUserRole = decoded.role.toLowerCase();

        if (currentUserRole != permissionFor.toLowerCase()) {
            console.log("hi");
            bootbox.alert({
                size: "large",
                message: "<h3>Authorization/Authentication Failed</h3><br>You do not have permissions to access the page.",
            });
            $(".breadcrumbs ol").empty();
            $(".breadcrumbs ol").append("<a href='index.html'><h2>Home </h2></a>");
            window.location.href = "#home";
            return false;
        }
        return true;
    }
    // general function end

    Handlebars.registerHelper("formatDate", function(date) {
        return moment().format("Do MMMM YYYY", date);
    });

    // Bookings Helpers start
    Handlebars.registerHelper("ifeq", function(a, b, options) {
        if (a == b) {
            return options.fn(this);
        }
        return options.inverse(this);
    });

    Handlebars.registerHelper("checkBookingsLength", function(
        bookings,
        options
    ) {
        if (bookings.length == 0) return options.fn(this);
        else return options.inverse(this);
    });

    Handlebars.registerHelper("getBookingStatus", function(status) {
        const fixedStatus = {
            0: "Pending",
            1: "Rejected",
            2: "Approved",
        };

        return fixedStatus[status];
    });

    // Rooms Helpers start
    Handlebars.registerHelper("displayRoomStatus", function(value) {
        if (value == 0) {
            return "Available";
        } else if (value == 1) {
            return "Not Available";
        } else {
            return "In Maintainance";
        }
    });

    // Rooms Helpers end

    //Guest Register
    var routeRegister = crossroads.addRoute("/register", function() {
        if (sessionStorage.token) {
            bootbox.alert(
                "You already signup. Please logout for another registration."
            );
            window.location.href = "#home";
            return;
        }
        var registerTemplate = Handlebars.templates["register"];
        $("#divcontent").empty();
        $("#divcontent").html(registerTemplate).hide().fadeIn(1000);

        $(".breadcrumbs ol").empty();
    });

    //User Login
    var routeLogin = crossroads.addRoute("/login", function() {
        if (sessionStorage.token) {
            window.location.href = "#home";
            return;
        }
        var loginTemplate = Handlebars.templates["login"];
        $("#divcontent").empty();
        $("#divcontent").html(loginTemplate).hide().fadeIn(1000);

        $(".breadcrumbs ol").empty();
    });

    var routelogout = crossroads.addRoute("/logout", function() {
        $("#loginname").html("Login");
        sessionStorage.removeItem("token");
        window.location.href = "#home";
        return;
    });

    var routeDefaultHome = crossroads.addRoute("", function() {
        if (!sessionStorage.token) {
            $("#loginname").html("Login");
            sessionStorage.removeItem("token");
            window.location.href = "#login";
            return;
        }

        var token = sessionStorage.getItem("token");

        var decoded = jwt_decode(token);
        $("#loginname").html(decoded.login);

        $("#footer").empty();

        $(".breadcrumbs ol").empty();
        $(".breadcrumbs ol").append("<li class='breadcrumb-item'>Home</li>");

        if (decoded.role.toLowerCase() == "student") {
            $.ajax({
                type: "GET",
                url: `api/users/${decoded.userid}/bookings`,
                dataType: "json",
                success: function(data) {
                    var status = "none";

                    for (var i = 0; i < data.length; i++) {
                        if (data[i].applicationStatus === 2) {
                            status = "approved";
                            break;
                        } else if (data[i].applicationStatus === 0) {
                            status = "pending";
                        }
                    }
                    var homeTemplate = Handlebars.templates["student-home"]({
                        data: status,
                    });
                    $("#divcontent").empty();
                    $("#divcontent").html(homeTemplate).hide().fadeIn(1000);
                },
                error: function(xhr) {
                    if (xhr.status == 401) {
                        var responseText = JSON.parse(xhr.responseText);
                        bootbox.alert("Error 401 - Unauthorized: " + responseText.message);

                        $("#loginname").html("noname");
                        sessionStorage.removeItem("token");
                        sessionStorage.removeItem("login");
                        window.location.href = "#login";
                        return;
                    }

                    if (xhr.status == 404) {
                        bootbox.alert("Error 404 - API resource not found at the server");
                    }
                },
            });
        } else {
            var homeTemplate = Handlebars.templates["admin-home"]();
            $("#divcontent").empty();
            $("#divcontent").html(homeTemplate).hide().fadeIn(1000);
        }
    });

    var routeHome = crossroads.addRoute("/home", function() {
        if (!sessionStorage.token) {
            $("#loginname").html("Login");
            sessionStorage.removeItem("token");
            window.location.href = "#login";
            return;
        }

        var token = sessionStorage.getItem("token");

        var decoded = jwt_decode(token);
        $("#loginname").html(decoded.login);

        $("#footer").empty();
        $(".breadcrumbs ol").empty();
        $(".breadcrumbs ol").append("<li class='breadcrumb-item'>Home</li>");

        if (decoded.role.toLowerCase() == "student") {
            $.ajax({
                type: "GET",
                // url: "api/getapplicationlist/" + decoded.userid,
                url: `api/users/${decoded.userid}/bookings`,
                dataType: "json",
                success: function(data) {
                    var status = "none";

                    for (var i = 0; i < data.length; i++) {
                        if (data[i].applicationStatus === 2) {
                            status = "approved";
                            break;
                        } else if (data[i].applicationStatus === 0) {
                            status = "pending";
                        }
                    }
                    var homeTemplate = Handlebars.templates["student-home"]({
                        data: status,
                    });
                    $("#divcontent").empty();
                    $("#divcontent").html(homeTemplate).hide().fadeIn(1000);
                },
                error: function(xhr) {
                    if (xhr.status == 401) {
                        var responseText = JSON.parse(xhr.responseText);
                        bootbox.alert("Error 401 - Unauthorized: " + responseText.message);

                        $("#loginname").html("noname");
                        sessionStorage.removeItem("token");
                        sessionStorage.removeItem("login");
                        window.location.href = "#login";
                        return;
                    }

                    if (xhr.status == 404) {
                        bootbox.alert("Error 404 - API resource not found at the server");
                    }
                },
            });
        } else {
            var homeTemplate = Handlebars.templates["admin-home"]();
            $("#divcontent").empty();
            $("#divcontent").html(homeTemplate).hide().fadeIn(1000);
        }
    });

    //User Profile Start//

    //Get Profile
    var routeProfile = crossroads.addRoute("/profile", function() {
        if (!sessionStorage.token) {
            $("#loginname").html("Login");
            sessionStorage.removeItem("token");
            window.location.href = "#login";
            return;
        }

        var token = sessionStorage.getItem("token");

        var decoded = jwt_decode(token);
        $("#loginname").html(decoded.login);

        $("#footer").empty();
        $(".breadcrumbs ol").empty();
        $(".breadcrumbs ol").append(
            "<li class='breadcrumb-item'><a href='#home'>Home</a></li>"
        );
        $(".breadcrumbs ol").append("<li class='breadcrumb-item'>My Profile</li>");

        $.ajax({
            type: "GET",
            url: "api/users",
            dataType: "json",
            success: function(data) {
                var profileTemplate = Handlebars.templates["profile"](data);

                $("#divcontent").empty();
                $("#divcontent").html(profileTemplate).hide().fadeIn(1000);
            },
            error: function(xhr, statusText, err) {
                if (xhr.status == 401) {
                    var responseText = JSON.parse(xhr.responseText);
                    bootbox.alert("Error 401 - Unauthorized: " + responseText.message);

                    $("#loginname").html("Login");
                    sessionStorage.removeItem("token");
                    sessionStorage.removeItem("login");
                    window.location.href = "#login";
                    return;
                }

                if (xhr.status == 404) {
                    bootbox.alert("Error 404 - API resource not found at the server");
                }
            },
        });
    });

    //Edit Profile
    var routeEditProfile = crossroads.addRoute(
        "/profile/editprofile",
        function() {
            if (!sessionStorage.token) {
                $("#loginname").html("Login");
                sessionStorage.removeItem("token");
                window.location.href = "#login";
                return;
            }

            var token = sessionStorage.getItem("token");

            var decoded = jwt_decode(token);

            $("#loginname").html(decoded.login);

            $("#footer").empty();
            $(".breadcrumbs ol").empty();
            $(".breadcrumbs ol").append(
                "<li class='breadcrumb-item'><a href='#home' >Home</a></li>"
            );

            $(".breadcrumbs ol").append(
                "<li class='breadcrumb-item'><a href='#profile'>My Profile</a></li>"
            );
            $(".breadcrumbs ol").append(
                "<li class='breadcrumb-item'>Edit Profile</li>"
            );

            $.ajax({
                type: "GET",
                url: "api/users",
                dataType: "json",
                success: function(data) {
                    var profileEditTemplate = Handlebars.templates["profile-edit"](data);

                    $("#divcontent").empty();
                    $("#divcontent").html(profileEditTemplate).hide().fadeIn(1000);
                },
                error: function(xhr, statusText, err) {
                    if (xhr.status == 401) {
                        var responseText = JSON.parse(xhr.responseText);
                        bootbox.alert("Error 401 - Unauthorized: " + responseText.message);

                        $("#loginname").html("Login");
                        sessionStorage.removeItem("token");
                        sessionStorage.removeItem("login");
                        window.location.href = "#login";
                        return;
                    }

                    if (xhr.status == 404) {
                        bootbox.alert("Error 404 - API resource not found at the server");
                    }
                },
            });
        }
    );

    //Reset Password
    var routeResetPassword = crossroads.addRoute(
        "/profile/passwordreset",
        function() {
            if (!sessionStorage.token) {
                $("#loginname").html("Login");
                sessionStorage.removeItem("token");
                window.location.href = "#login";
                return;
            }

            //get token from sessionStorage
            var token = sessionStorage.getItem("token");

            //decode jwt token using jwt-decode
            var decoded = jwt_decode(token);

            $("#loginname").html(decoded.login);
            context = {
                login: decoded.login,
            };
            var profilePassResetTemplate = Handlebars.templates[
                "profile-passwordreset"
            ](context);

            $("#footer").empty();
            $(".breadcrumbs ol").empty();
            $(".breadcrumbs ol").append(
                "<li class='breadcrumb-item'><a href='#home'>Home</a></li>"
            );
            $(".breadcrumbs ol").append(
                "<li  class='breadcrumb-item'><a href='#profile'>My Profile</a></li>"
            );
            $(".breadcrumbs ol").append(
                "<li class='breadcrumb-item'>Reset Password</li>"
            );
            $("#divcontent").empty();
            $("#divcontent").html(profilePassResetTemplate).hide().fadeIn(1000);

            // refresh the token for another 15 minutes
            $.ajax({
                type: "post",
                url: "api/refreshtoken",
                dataType: "json",
                success: function(data) {
                    sessionStorage.setItem("token", data.token);
                    console.log("refresh token: " + data.token);
                },
                error: function(xhr, statusText, err) {
                    if (xhr.status == 401) {
                        //response text from the server if there is any
                        var responseText = JSON.parse(xhr.responseText);
                        bootbox.alert("Error 401 - Unauthorized: " + responseText.message);

                        $("#loginname").html("Login");
                        sessionStorage.removeItem("token");
                        sessionStorage.removeItem("login");
                        window.location.href = "#login";
                        return;
                    }

                    if (xhr.status == 404) {
                        bootbox.alert("Error 404 - API resource not found at the server");
                    }
                },
            });
        }
    );

    //Change Profile Photo
    var routeChangePhoto = crossroads.addRoute(
        "/profile/edit/changephoto",
        function(id) {
            if (!sessionStorage.token) {
                $("#loginname").html("Login");
                sessionStorage.removeItem("token");
                window.location.href = "#login";
                return;
            }

            var token = sessionStorage.getItem("token");

            var decoded = jwt_decode(token);
            $("#loginname").html(decoded.login);

            $("#footer").empty();
            $(".breadcrumbs ol").empty();
            $(".breadcrumbs ol").append(
                "<li class='breadcrumb-item'><a href='#home'>Home</a></li>"
            );
            $(".breadcrumbs ol").append(
                "<li class='breadcrumb-item'><a href='#profile'>My Profile</a></li>"
            );
            $(".breadcrumbs ol").append(
                "<li class='breadcrumb-item'>Change Photo</li>"
            );
            $.ajax({
                type: "get",
                url: "api/users",
                dataType: "json",
                success: function(data) {
                    if (!$.isEmptyObject(data)) {
                        var profileUploadPhoto = Handlebars.templates[
                            "profile-upload-photo"
                        ](data);

                        $("#divcontent").empty();
                        $("#divcontent").html(profileUploadPhoto).hide().fadeIn(1000);
                    } else {
                        bootbox.alert("Upload Photo Failed. Please Try Again!");
                        window.location.href = "#home";
                    }
                },
                error: function(xhr, statusText, err) {
                    if (xhr.status == 401) {
                        var responseText = JSON.parse(xhr.responseText);
                        bootbox.alert("Error 401 - Unauthorized: " + responseText.message);

                        $("#loginname").html("noname");
                        sessionStorage.removeItem("token");
                        sessionStorage.removeItem("login");
                        window.location.href = "#login";
                        return;
                    }

                    if (xhr.status == 404) {
                        bootbox.alert("Error 404 - API resource not found at the server");
                    }
                },
            });
        }
    );

    //User Profile End//

    var routeAdminBookings = crossroads.addRoute("/admin-bookings", function() {
        if (!sessionStorage.token) {
            $("#loginname").html("Login");
            sessionStorage.removeItem("token");
            window.location.href = "#login";
            return;
        }
        //Only for specific to load the page, else, return to home and alert
        if (!checkUserRole("staff")) return;
        //get token from sessionStorage
        var token = sessionStorage.getItem("token");

        //decode jwt token using jwt-decode
        var decoded = jwt_decode(token);
        $("#loginname").html(decoded.login);

        $("#footer").empty();

        var bookingsTemplate;
        $.ajax({
            type: "get",
            url: "api/bookings",
            contentType: "application/json",
            dataType: "json",
            success: function(data) {
                var statistics = { Pending: [], Approve: [], Reject: [] };
                data.forEach((booking) => {
                    switch (booking.applicationStatus) {
                        case 0:
                            statistics.Pending.push(booking);
                            break;
                        case 1:
                            statistics.Reject.push(booking);
                            break;
                        case 2:
                            statistics.Approve.push(booking);
                            break;
                    }
                });
                bookingsTemplate = Handlebars.templates["admin-bookings"]({
                    bookings: statistics,
                });

                $("#divcontent").empty();
                $("#divcontent").html(bookingsTemplate).hide().fadeIn(1000);
                $(".breadcrumbs ol").empty();
                $(".breadcrumbs ol").append(
                    "<li class='breadcrumb-item'><a href='#home'>Home</a></li>"
                );
                $(".breadcrumbs ol").append(
                    "<li class='breadcrumb-item'>Bookings</li>"
                );
            },
            error: function(xhr, statusText, err) {
                if (xhr.status == 401) {
                    //response text from the server if there is any
                    var responseText = JSON.parse(xhr.responseText);
                    bootbox.alert("Error 401 - Unauthorized: " + responseText.message);

                    $("#loginname").html("Login");
                    sessionStorage.removeItem("token");
                    sessionStorage.removeItem("login");
                    window.location.href = "#login";
                    return;
                }

                if (xhr.status == 404) {
                    bootbox.alert("Error 404 - API resource not found at the server");
                }
            },
        });
    });

    var routeAdminBookingEdit = crossroads.addRoute(
        "/admin-bookings/edit/{id}",
        function(id) {
            if (!sessionStorage.token) {
                $("#loginname").html("Login");
                sessionStorage.removeItem("token");
                window.location.href = "#login";
                return;
            }

            //Only for specific to load the page, else, return to home and alert
            if (!checkUserRole("staff")) return;

            //get token from sessionStorage
            var token = sessionStorage.getItem("token");

            //decode jwt token using jwt-decode
            var decoded = jwt_decode(token);
            $("#loginname").html(decoded.login);

            $("#footer").empty();

            var bookingTemplate;
            $.ajax({
                type: "get",
                url: "api/bookings/" + id,
                contentType: "application/json",
                dataType: "json",
                success: function(data) {
                    bookingTemplate = Handlebars.templates["admin-booking-edit"]({
                        booking: data,
                    });

                    $("#divcontent").empty();
                    $("#divcontent").html(bookingTemplate).hide().fadeIn(1000);
                    $(".breadcrumbs ol").empty();
                    $(".breadcrumbs ol").append(
                        "<li class='breadcrumb-item'><a href='#home'>Home</a></li>"
                    );
                    $(".breadcrumbs ol").append(
                        "<li class='breadcrumb-item'><a href='#admin-bookings'>Bookings</a></li>"
                    );
                    $(".breadcrumbs ol").append(
                        "<li class='breadcrumb-item'>Edit Booking</li>"
                    );
                },
                error: function(xhr, statusText, err) {
                    if (xhr.status == 401) {
                        //response text from the server if there is any
                        var responseText = JSON.parse(xhr.responseText);
                        bootbox.alert("Error 401 - Unauthorized: " + responseText.message);

                        $("#loginname").html("Login");
                        sessionStorage.removeItem("token");
                        sessionStorage.removeItem("login");
                        window.location.href = "#login";
                        return;
                    }

                    if (xhr.status == 404) {
                        bootbox.alert("Error 404 - API resource not found at the server");
                    }
                },
            });
        }
    );

    var routeAdminBookingView = crossroads.addRoute(
        "/admin-bookings/view/{id}",
        function(id) {
            if (!sessionStorage.token) {
                $("#loginname").html("Login");
                sessionStorage.removeItem("token");
                window.location.href = "#login";
                return;
            }

            //Only for specific to load the page, else, return to home and alert
            if (!checkUserRole("staff")) return;

            //get token from sessionStorage
            var token = sessionStorage.getItem("token");

            //decode jwt token using jwt-decode
            var decoded = jwt_decode(token);
            $("#loginname").html(decoded.login);

            $("#footer").empty();
            var bookingTemplate;
            $.ajax({
                type: "get",
                url: "api/bookings/" + id,
                contentType: "application/json",
                dataType: "json",
                success: function(data) {
                    bookingTemplate = Handlebars.templates["admin-booking-view"]({
                        booking: data,
                    });

                    $("#divcontent").empty();
                    $("#divcontent").html(bookingTemplate).hide().fadeIn(1000);
                    $(".breadcrumbs ol").empty();
                    $(".breadcrumbs ol").append(
                        "<li class='breadcrumb-item'><a href='#home'>Home</a></li>"
                    );
                    $(".breadcrumbs ol").append(
                        "<li class='breadcrumb-item'><a href='#admin-bookings'>Bookings</a></li>"
                    );
                    $(".breadcrumbs ol").append(
                        "<li class='breadcrumb-item'>View Booking</li>"
                    );
                },
                error: function(xhr, statusText, err) {
                    if (xhr.status == 401) {
                        //response text from the server if there is any
                        var responseText = JSON.parse(xhr.responseText);
                        bootbox.alert("Error 401 - Unauthorized: " + responseText.message);

                        $("#loginname").html("Login");
                        sessionStorage.removeItem("token");
                        sessionStorage.removeItem("login");
                        window.location.href = "#login";
                        return;
                    }

                    if (xhr.status == 404) {
                        bootbox.alert("Error 404 - API resource not found at the server");
                    }
                },
            });
        }
    );

    var routeRoom1 = crossroads.addRoute("/rooms", function() {
        if (!sessionStorage.token) {
            $("#loginname").html("Login");
            sessionStorage.removeItem("token");
            window.location.href = "#login";
            return;
        }

        if(!checkUserRole("staff")) return;

        //get token from sessionStorage
        var token = sessionStorage.getItem("token");

        //decode jwt token using jwt-decode
        var decoded = jwt_decode(token);
        $("#loginname").html(decoded.login);

        $("#footer").empty();

        $.ajax({
            type: "get",
            url: "api/rooms",
            dataType: "json",
            success: function(data) {
                $("#divcontent").empty();
                    var adminManageRoomsViewTemplate = Handlebars.templates[
                        "admin-manage-rooms-view"
                    ]({
                        rooms: data,
                    });
                    $("#divcontent")
                        .html(adminManageRoomsViewTemplate)
                        .hide()
                        .fadeIn(1000);
            },
            error: function(xhr, statusText, err) {
                if (xhr.status == 401) {
                    //response text from the server if there is any
                    var responseText = JSON.parse(xhr.responseText);
                    bootbox.alert("Error 401 - Unauthorized: " + responseText.message);

                    $("#loginname").html("Login");
                    sessionStorage.removeItem("token");
                    sessionStorage.removeItem("login");
                    window.location.href = "#login";
                    return;
                }

                if (xhr.status == 404) {
                    bootbox.alert("Error 404 - API resource not found at the server");
                }
            },
        });
        $(".breadcrumbs ol").empty();
        $(".breadcrumbs ol").append(
            "<li class='breadcrumb-item'><a href='#home'>Home</a></li>"
        );
        $(".breadcrumbs ol").append("<li class='breadcrumb-item'>Rooms</li>");
    });

    var routeRoom2 = crossroads.addRoute("/rooms/{id}", function(id) {
        if (!sessionStorage.token) {
            $("#loginname").html("Login");
            sessionStorage.removeItem("token");
            window.location.href = "#login";
            return;
        }

        if(!checkUserRole("staff")) return;

        //get token from sessionStorage
        var token = sessionStorage.getItem("token");

        //decode jwt token using jwt-decode
        var decoded = jwt_decode(token);
        $("#loginname").html(decoded.login);

        $("#footer").empty();

        $.ajax({
            type: "get",
            url: `api/rooms/${id}`,
            dataType: "json",
            success: function(data) {
                $("#divcontent").empty();

                    var adminManageRoomViewDetailsTemplate = Handlebars.templates[
                        "admin-manage-room-view-details"
                    ](data);
                    $("#divcontent")
                        .html(adminManageRoomViewDetailsTemplate)
                        .hide()
                        .fadeIn(1000);
            },
            error: function(xhr, statusText, err) {
                if (xhr.status == 401) {
                    var responseText = JSON.parse(xhr.responseText);
                    //response text from the server if there is any
                    bootbox.alert("Error 401 - Unauthorized: " + responseText.message);

                    $("#loginname").html("Login");
                    sessionStorage.removeItem("token");
                    sessionStorage.removeItem("login");
                    window.location.href = "#login";
                    return;
                }

                if (xhr.status == 404) {
                    bootbox.alert("Error 404 - API resource not found at the server");
                } else if (xhr.status == 500) {
                    bootbox.alert("Error 500 - " + responseText);
                }
            },
        });
        $(".breadcrumbs ol").empty();
        $(".breadcrumbs ol").append(
            "<li class='breadcrumb-item'><a href='#home'>Home</a></li>"
        );
        $(".breadcrumbs ol").append(
            "<li class='breadcrumb-item'><a href='#rooms'>Rooms</a></li>"
        );
        $(".breadcrumbs ol").append("<li class='breadcrumb-item'>View</li>");
    });

    var routeRoom5 = crossroads.addRoute("/rooms/edit/{id}", function(id) {
        if (!sessionStorage.token) {
            $("#loginname").html("noname");
            sessionStorage.removeItem("token");
            window.location.href = "#login";
            return;
        }

        //Only for specific to load the page, else, return to home and alert
        if (!checkUserRole("staff")) return;

        $.ajax({
            type: "get",
            url: "api/rooms/edit/" + id,
            dataType: "json",
            success: function(data) {
                var adminManageRoomEditTemplate = Handlebars.templates[
                    "admin-manage-room-edit"
                ](data);
                $("#divcontent").empty();
                $("#divcontent").html(adminManageRoomEditTemplate).hide().fadeIn(1000);
            },
            error: function(xhr, statusText, err) {
                if (xhr.status == 401) {
                    var responseText = JSON.parse(xhr.responseText);
                    //response text from the server if there is any
                    bootbox.alert("Error 401 - Unauthorized: " + responseText.message);

                    $("#loginname").html("Login");
                    sessionStorage.removeItem("token");
                    sessionStorage.removeItem("login");
                    window.location.href = "#login";
                    return;
                }

                if (xhr.status == 404) {
                    bootbox.alert("Error 404 - API resource not found at the server");
                } else if (xhr.status == 500) {
                    bootbox.alert("Error 500 - " + responseText);
                }
            },
        });
        $(".breadcrumbs ol").empty();
        $(".breadcrumbs ol").append(
            "<li class='breadcrumb-item'><a href='#home'>Home</a></li>"
        );
        $(".breadcrumbs ol").append(
            "<li class='breadcrumb-item'><a href='#rooms'>Rooms</a></li>"
        );
        $(".breadcrumbs ol").append(
            "<li class='breadcrumb-item'>View Room</a></li>"
        );
    });

    //add application
    var routeBookingroom = crossroads.addRoute("/bookingroom", function() {
        if (!sessionStorage.token) {
            $("#loginname").html("Login");
            sessionStorage.removeItem("token");
            window.location.href = "#login";
            return;
        }

        //Only for specific to load the page, else, return to home and alert
        if (!checkUserRole("student")) return;

        //get token from sessionStorage
        var token = sessionStorage.getItem("token");

        //decode jwt token using jwt-decode
        var decoded = jwt_decode(token);
        $("#loginname").html(decoded.login);

        $("#footer").empty();

        $(".breadcrumbs ol").empty();
        $(".breadcrumbs ol").append(
            "<li class='breadcrumb-item'><a href='#home'>Home</a></li>"
        );
        $(".breadcrumbs ol").append(
            "<li class='breadcrumb-item'>Add Application</li>"
        );
        Handlebars.registerHelper("formatDate", function(date) {
            return moment(date).format("Do MMMM YYYY");
        });

        $.ajax({
            type: "get",
            url: "api/rooms",
            dataType: "json",
            success: function(data) {
                var addApplicationTemplate = Handlebars.templates[
                    "student-application-add"
                ]({
                    roomlist: data,
                });
                $("#divcontent").empty();
                $("#divcontent").html(addApplicationTemplate).hide().fadeIn(1000);
                $('#tblrooms tbody tr').each(function(i) {
                    var tdIndex = $(this).children().eq(0);

                    var id = data[i].id;
                    var roomNo = data[i].roomNo;
                    var roomType = data[i].roomType;
                    var roomStatus = data[i].roomStatus;
                    var bedArrangement = data[i].bedArrangement;
                    var block = data[i].block;
                    var college = data[i].college;
                    var lastModifiedDate = data[i].lastModifiedDate;


                    $(tdIndex).data("id", id);
                    $(tdIndex).data("roomNo", roomNo);
                    $(tdIndex).data("roomType", roomType);
                    $(tdIndex).data("roomStatus", roomStatus);
                    $(tdIndex).data("bedArrangement", bedArrangement);
                    $(tdIndex).data("block", block);
                    $(tdIndex).data("college", college);
                    $(tdIndex).data("lastModifiedDate", lastModifiedDate);


                });
            },
            error: function(xhr, statusText, err) {
                if (xhr.status == 401) {
                    //response text from the server if there is any
                    var responseText = JSON.parse(xhr.responseText);
                    bootbox.alert("Error 401 - Unauthorized: " + responseText.message);

                    $("#loginname").html("Login");
                    sessionStorage.removeItem("token");
                    sessionStorage.removeItem("login");
                    window.location.href = "#login";
                    return;
                }

                if (xhr.status == 404) {
                    bootbox.alert("Error 404 - API resource not found at the server");
                }
            },
        });
    });

    var routeViewApplication = crossroads.addRoute(
        "/viewapplication",
        function() {
            if (!sessionStorage.token) {
                $("#loginname").html("Login");
                sessionStorage.removeItem("token");
                window.location.href = "#login";
                return;
            }

            //Only for specific to load the page, else, return to home and alert
            if (!checkUserRole("student")) return;

            //get token from sessionStorage
            var token = sessionStorage.getItem("token");

            //decode jwt token using jwt-decode
            var decoded = jwt_decode(token);
            $("#loginname").html(decoded.login);

            $("#footer").empty();
            $(".breadcrumbs ol").empty();
            $(".breadcrumbs ol").append(
                "<li class='breadcrumb-item'><a href='#home'>Home</a></li>"
            );
            $(".breadcrumbs ol").append(
                "<li class='breadcrumb-item'>View Application</li>"
            );
            $("#divcontent").empty();

            Handlebars.registerHelper("checkApplicationLength", function(
                applicationlist,
                options
            ) {
                // console.log(applicationlist.length);
                if (applicationlist.length == 0) return options.fn(this);
                else return options.inverse(this);
            });
            Handlebars.registerHelper("displayRemark", function(value) {
                if (value === "") {
                    return "No remarks";
                } else {
                    return value;
                }
            });

            Handlebars.registerHelper("displayRoomType", function(value) {
                if (value == 0) {
                    return "Single";
                } else if (value == 1) {
                    return "Double";
                } else {
                    return "Deluxe Single";
                }
            });
            Handlebars.registerHelper("formatDate", function(date) {
                return moment(date).format("Do MMMM YYYY");
            });

            Handlebars.registerHelper("inc", function(value) {
                return parseInt(value) + 1;
            });
            Handlebars.registerHelper("displayStatus", function(value) {
                if (value == 0) {
                    return "Pending";
                } else if (value == 1) {
                    return "Rejected";
                } else {
                    return "Approved";
                }
            });
            $.ajax({
                type: "GET",
                // url: "api/getapplicationlist/" + userid,
                url: `api/users/${decoded.userid}/bookings`,

                dataType: "json",
                success: function(data) {
                    var studentApplicationTemplate = Handlebars.templates[
                        "student-application"
                    ]({ applicationlist: data });
                    $("#divcontent").html(studentApplicationTemplate).hide().fadeIn(1000);
                    $("#page_title").html("View Application");
                },
                error: function(xhr, statusText, err) {
                    if (xhr.status == 401) {
                        //response text from the server if there is any
                        var responseText = JSON.parse(xhr.responseText);
                        bootbox.alert("Error 401 - Unauthorized: " + responseText.message);

                        $("#loginname").html("Login");
                        sessionStorage.removeItem("token");
                        sessionStorage.removeItem("login");
                        window.location.href = "#login";
                        return;
                    }

                    if (xhr.status == 404) {
                        bootbox.alert("Error 404 - API resource not found at the server");
                    }
                },
            });
        }
    );

    //view application detail

    var routeApplicationDetails = crossroads.addRoute(
        "/application/view/{id}",
        function(id) {
            if (!sessionStorage.token) {
                $("#loginname").html("Login");
                sessionStorage.removeItem("token");
                window.location.href = "#login";
                return;
            }
            var token = sessionStorage.getItem("token");

            //decode jwt token using jwt-decode
            var decoded = jwt_decode(token);
            $("#loginname").html(decoded.login);

            //Only for specific to load the page, else, return to home and alert
            if (!checkUserRole("student")) return;

            $("#footer").empty();
            $(".breadcrumbs ol").empty();
            $(".breadcrumbs ol").append(
                "<li class='breadcrumb-item'><a href='#home'>Home</a></li>"
            );
            $(".breadcrumbs ol").append(
                "<li class='breadcrumb-item'><a href='#viewapplication'>View Application</a></li>"
            );
            $(".breadcrumbs ol").append(
                "<li class='breadcrumb-item'>Application Detail</li>"
            );
            $("#divcontent").empty();

            Handlebars.registerHelper("displayRemark", function(value) {
                if (value === "") {
                    return "No remarks";
                } else {
                    return value;
                }
            });
            Handlebars.registerHelper("displayStatus", function(value) {
                if (value == 0) {
                    return "Pending";
                } else if (value == 1) {
                    return "Rejected";
                } else {
                    return "Approved";
                }
            });
            Handlebars.registerHelper("formatDate", function(date) {
                return moment(date).format("Do MMMM YYYY");
            });
            Handlebars.registerHelper("displayRoomType", function(value) {
                if (value == 0) {
                    return "Single";
                } else if (value == 1) {
                    return "Double";
                } else {
                    return "Deluxe Single";
                }
            });
            $.ajax({
                type: "GET",
                // url: "api/getapplicationlist/" + userid,
                url: `api/users/${decoded.userid}/bookings`,
                dataType: "json",
                success: function(data) {
                    var selectIndex = "";
                    for (var i = 0; i < data.length; i++) {
                        if (data[i].id == id) {
                            selectIndex = i;
                        }
                    }
                    console.log(data[selectIndex]);
                    var studentApplicationDetailTemplate = Handlebars.templates[
                        "student-application-view"
                    ]({ application: data[selectIndex] });
                    $("#divcontent")
                        .html(studentApplicationDetailTemplate)
                        .hide()
                        .fadeIn(1000);
                    $("#page_title").html("Application Detail");
                },
                error: function(xhr, statusText, err) {
                    if (xhr.status == 401) {
                        //response text from the server if there is any
                        var responseText = JSON.parse(xhr.responseText);
                        bootbox.alert("Error 401 - Unauthorized: " + responseText.message);

                        $("#loginname").html("Login");
                        sessionStorage.removeItem("token");
                        sessionStorage.removeItem("login");
                        window.location.href = "#login";
                        return;
                    }

                    if (xhr.status == 404) {
                        bootbox.alert("Error 404 - API resource not found at the server");
                    }
                },
            });
        }
    );

    //view student
    var routeViewStudent = crossroads.addRoute("/students", function() {
        if (!sessionStorage.token) {
            $("#loginname").html("Login");
            sessionStorage.removeItem("token");
            window.location.href = "#login";
            return;
        }

        //Only for specific to load the page, else, return to home and alert
        if (!checkUserRole("staff")) return;

        //get token from sessionStorage
        var token = sessionStorage.getItem("token");

        //decode jwt token using jwt-decode
        var decoded = jwt_decode(token);
        $("#loginname").html(decoded.login);

        $("#footer").empty();

        $(".breadcrumbs ol").empty();
        $(".breadcrumbs ol").append(
            "<li class='breadcrumb-item'><a href='#home' >Home</a></li>"
        );
        $(".breadcrumbs ol").append(
            "<li class='breadcrumb-item'>Students List</li>"
        );

        $.ajax({
            type: "get",
            url: "api/users/students",
            dataType: "json",
            success: function(data) {
                console.log(data);
                var studentListTemplate = Handlebars.templates["admin-student-list"]({
                    studentlist: data,
                });
                $("#divcontent").empty();
                $("#divcontent").html(studentListTemplate).hide().fadeIn(1000);
                // $("#page_title").html("Add Application");

                $("#tblstudent tbody tr").each(function(i) {
                    var tdIndex = $(this).children().eq(0);

                    var id = data[i].id;

                    var uni_id = data[i].uniID;
                    var name = data[i].name;
                    var email = data[i].email;
                    var mobileno = data[i].mobileno;

                    $(tdIndex).data("id", id);
                    $(tdIndex).data("uni_id", uni_id);
                    $(tdIndex).data("name", name);
                    $(tdIndex).data("email", email);
                    $(tdIndex).data("mobileno", mobileno);
                });
            },
            error: function(xhr, statusText, err) {
                if (xhr.status == 401) {
                    //response text from the server if there is any
                    var responseText = JSON.parse(xhr.responseText);
                    bootbox.alert("Error 401 - Unauthorized: " + responseText.message);

                    $("#loginname").html("noname");
                    sessionStorage.removeItem("token");
                    sessionStorage.removeItem("login");
                    window.location.href = "#login";
                    return;
                }

                if (xhr.status == 404) {
                    bootbox.alert("Error 404 - API resource not found at the server");
                }
            },
        });
    });

    hasher.initialized.add(parseHash);
    hasher.changed.add(parseHash);
    hasher.init();
});