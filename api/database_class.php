<?php

$roomTypeArray = ['Single', 'Double', 'Deluxe Single'];

class User
{
   var $id;
   var $uniID;
   var $username;
   var $passwordhash;
   var $name;
   var $email;
   var $mobileno;
   var $photo;
   var $userType;
   var $addedDate;
}

abstract class StudentAbstract extends User{}

abstract class StaffAbstract extends User{}

class Student extends StudentAbstract{}

class Staff extends StaffAbstract {}

class Booking
{
   var $id;
   var $room;
   var $user;
   var $applicationStatus;
   var $remark;
   var $applyDate;
   var $lastModifiedDate;
}

class Room
{
   var $id;
   var $roomNo;
   var $roomType;
   var $roomStatus;
   var $bedArrangement;
   var $block;
   var $college;
   var $lastModifiedDate;

   function getRoomType($roomTypeIndex){
      try {
         global $roomTypeArray;
         return $roomTypeArray[$roomTypeIndex];
      } catch (Exception $e) {
         return "Trollol Room Type";
      }
   }
}

class DbStatus
{
   var $status;
   var $error;
   var $lastinsertid;
}

function time_elapsed_string($datetime, $full = false)
{

   if ($datetime == '0000-00-00 00:00:00')
      return "none";

   if ($datetime == '0000-00-00')
      return "none";

   $now = new DateTime;
   $ago = new DateTime($datetime);
   $diff = $now->diff($ago);

   $diff->w = floor($diff->d / 7);
   $diff->d -= $diff->w * 7;

   $string = array(
      'y' => 'year',
      'm' => 'month',
      'w' => 'week',
      'd' => 'day',
      'h' => 'hour',
      'i' => 'minute',
      's' => 'second',
   );

   foreach ($string as $k => &$v) {
      if ($diff->$k) {
         $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
      } else {
         unset($string[$k]);
      }
   }

   if (!$full) $string = array_slice($string, 0, 1);
   return $string ? implode(', ', $string) . ' ago' : 'just now';
}

function hashPassword($password)
{

   $cost = 10;

   $options = [
      'cost' => $cost,
   ];

   $passwordhash =  password_hash($password, PASSWORD_BCRYPT, $options);
   return $passwordhash;
}

class Database
{
   protected $dbhost;
   protected $dbuser;
   protected $dbpass;
   protected $dbname;
   protected $db;

   function __construct($dbhost, $dbuser, $dbpass, $dbname)
   {
      $this->dbhost = $dbhost;
      $this->dbuser = $dbuser;
      $this->dbpass = $dbpass;
      $this->dbname = $dbname;

      $db = new PDO("mysql:host=$dbhost;dbname=$dbname", $dbuser, $dbpass);
      $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
      $db->setAttribute(PDO::MYSQL_ATTR_FOUND_ROWS, true);
      $this->db = $db;
   }

   function beginTransaction()
   {
      try {
         $this->db->beginTransaction();
      } catch (PDOException $e) {
         $errorMessage = $e->getMessage();
         return $errorMessage;
      }
      return 1;
   }

   function commit()
   {
      try {
         $this->db->commit();
      } catch (PDOException $e) {
         $errorMessage = $e->getMessage();
         return $errorMessage;
      }
      return 1;
   }

   function rollback()
   {
      try {
         return $this->db->rollback();
      } catch (PDOException $e) {
         $errorMessage = $e->getMessage();
         return $errorMessage;
      }
      return 1;
   }

   function close()
   {
      try {
         $this->db = null;
      } catch (PDOException $e) {
         $errorMessage = $e->getMessage();
         return 0;
      }
   }

   function convertRoomType($roomTypeIndex)
   {
      try {
         global $roomTypeArray;
         return $roomTypeArray[$roomTypeIndex];
      } catch (Exception $e) {
         return "Trollol Room Type";
      }
   }

   function insertUser($username, $clearpassword, $name, $matric, $email, $mobile)
   {

      //hash the password using one way md5 brcrypt hashing
      $passwordhash = hashPassword($clearpassword);
      try {

         $sql = "INSERT INTO users(username, password, name, uni_id, email, mobileno, photo, user_type, addedDate) 
                    VALUES (:username, :password, :name, :uni_id, :email, :mobileno, 'default.png', 'Student', NOW())";

         $stmt = $this->db->prepare($sql);
         $stmt->bindParam("username", $username);
         $stmt->bindParam("password", $passwordhash);
         $stmt->bindParam("name", $name);
         $stmt->bindParam("uni_id", $matric);
         $stmt->bindParam("email", $email);
         $stmt->bindParam("mobileno", $mobile);
         $stmt->execute();

         $dbs = new DbStatus();
         $dbs->status = true;
         $dbs->error = "none";
         $dbs->lastinsertid = $this->db->lastInsertId();

         return $dbs;
      } catch (PDOException $e) {
         $errorMessage = $e->getMessage();

         $dbs = new DbStatus();
         $dbs->status = false;
         $dbs->error = $errorMessage;

         return $dbs;
      }
   }

   function checkemail($email)
   {
      $sql = "SELECT *
                 FROM users
                 WHERE email = :email";

      $stmt = $this->db->prepare($sql);
      $stmt->bindParam("email", $email);
      $stmt->execute();
      $row_count = $stmt->rowCount();
      return $row_count;
   }

   function authenticateUser($username)
   {
      $sql = "SELECT username, password as passwordhash, email, name, user_type, id
                 FROM users
                 WHERE username = :username";

      $stmt = $this->db->prepare($sql);
      $stmt->bindParam("username", $username);
      $stmt->execute();
      $row_count = $stmt->rowCount();

      $user = null;

      if ($row_count) {
         while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $user = new User();
            $user->username = $row['username'];
            $user->passwordhash = $row['passwordhash'];
            $user->email = $row['email'];
            $user->name = $row['name'];
            $user->userType = $row['user_type'];
            $user->userid = $row['id'];
         }
      }

      return $user;
   }



   ///////////////// User Profile Start //////////////////////

   //get single user via login
   function getUserViaLogin($username)
   {
      $sql = "SELECT *
                 FROM users
                 WHERE username = :username";

      $stmt = $this->db->prepare($sql);
      $stmt->bindParam("username", $username);
      $stmt->execute();
      $row_count = $stmt->rowCount();

      $user = new User();

      if ($row_count) {
         while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $user->id = $row['id'];
            $user->username = $row['username'];
            $user->name = $row['name'];
            $user->email = $row['email'];
            $user->mobileno = $row['mobileno'];
            $user->photo = $row['photo'];
            $user->uniID = $row['uni_id'];
            $user->userType = $row["user_type"];
            $addeddate = $row['addedDate'];
            $user->addedDate = time_elapsed_string($addeddate);
         }
      }

      return $user;
   }

   //update user via login
   function updateUserViaLogin($username, $name, $email, $mobileno)
   {

      $sql = "UPDATE users
                 SET name = :name,
                     email = :email,
                     mobileno = :mobileno
                 WHERE username = :username";

      try {
         $stmt = $this->db->prepare($sql);
         $stmt->bindParam("username", $username);
         $stmt->bindParam("name", $name);
         $stmt->bindParam("email", $email);
         $stmt->bindParam("mobileno", $mobileno);
         $stmt->execute();

         $dbs = new DbStatus();
         $dbs->status = true;
         $dbs->error = "none";

         return $dbs;
      } catch (PDOException $e) {
         $errorMessage = $e->getMessage();

         $dbs = new DbStatus();
         $dbs->status = false;
         $dbs->error = $errorMessage;

         return $dbs;
      }
   }

   //get user password via login
   function getUserPasswordViaLogin($username)
   {

      $sql = "SELECT password
                 FROM users
                 WHERE username = :username";

      $stmt = $this->db->prepare($sql);
      $stmt->bindParam("username", $username);
      $stmt->execute();
      $row_count = $stmt->rowCount();

      $password = "";

      if ($row_count) {
         while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $password = $row['password'];
         }
      }

      return $password;
   }

   //update user password via login
   function updateUserPasswordViaLogin($username, $clearpassword)
   {

      //hash the new password using one way md5 brcrypt encrypted hashing
      $passwordhash = hashPassword($clearpassword);

      $sql = "UPDATE users
                 SET password = :password
                 WHERE username = :username";

      try {
         $stmt = $this->db->prepare($sql);
         $stmt->bindParam("username", $username);
         $stmt->bindParam("password", $passwordhash);
         $stmt->execute();

         $dbs = new DbStatus();
         $dbs->status = true;
         $dbs->error = "none";

         return $dbs;
      } catch (PDOException $e) {
         $errorMessage = $e->getMessage();

         $dbs = new DbStatus();
         $dbs->status = false;
         $dbs->error = $errorMessage;

         return $dbs;
      }
   }

   //update profile photo
   function updateProfilePhoto($photo, $id)
   {

      $sql = "UPDATE users
                 SET photo = :photo
                 WHERE id = :id";

      try {
         $stmt = $this->db->prepare($sql);
         $stmt->bindParam("id", $id);
         $stmt->bindParam("photo", $photo);
         $stmt->execute();

         $dbs = new DbStatus();
         $dbs->status = true;
         $dbs->error = "none";

         return $dbs;
      } catch (PDOException $e) {
         $errorMessage = $e->getMessage();

         $dbs = new DbStatus();
         $dbs->status = false;
         $dbs->error = $errorMessage;

         return $dbs;
      }
   }

   ///////////////// User Profile End //////////////////////


   ///////////////// Student Start //////////////////////

   // get student list
   public function getStudents()
   {
      $sql = "SELECT *
              FROM users WHERE user_type = 'Student' ";
      //WHERE ownerlogin = :ownerlogin";
      $db = getDatabase();

      $stmt = $this->db->prepare($sql);
      $stmt->execute();
      $row_count = $stmt->rowCount();
      $data = array();

      if ($row_count) {
         while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $students = new Student();
            $students->id = $row['id'];
            $students->uniID = $row['uni_id'];
            $students->name = $row['name'];
            $students->email = $row['email'];
            $students->mobileno = $row['mobileno'];

            array_push($data, $students);
         }
      }

      return $data;
   }
   ///////////////// Student End //////////////////////


   ///////////////// Bookings Start //////////////////////

   //get all bookings
   public function getBookings()
   {
      $sql = "SELECT *
                    FROM booking";

      $stmt = $this->db->prepare($sql);
      $stmt->execute();
      $row_count = $stmt->rowCount();

      $bookings = [];

      if ($row_count) {
         while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $booking = new Booking();
            $booking->id = $row['id'];
            $booking->room = $this->getRoomById($row['room']);
            $booking->user = $this->getStudentById($row['user']);
            $booking->applicationStatus = $row['application_status'];
            $booking->remark = $row['remark'];
            $booking->applyDate = $row['apply_date'];
            $booking->lastModifiedDate = $row['modified_date'];

            array_push($bookings, $booking);
         }
      }

      return $bookings;
   }

   public function getBookingById($id)
   {
      $sql = "SELECT * FROM booking WHERE id = :id";

      $stmt = $this->db->prepare($sql);
      $stmt->bindParam('id', $id);
      $stmt->execute();
      $row_count = $stmt->rowCount();

      $booking = new Booking;

      if ($row_count) {
         while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $booking->id = $row['id'];
            $booking->room = $this->getRoomById($row['room']);
            $booking->user = $this->getStudentById($row['user']);
            $booking->applicationStatus = $row['application_status'];
            $booking->remark = $row['remark'];
            $booking->applyDate = $row['apply_date'];
            $booking->lastModifiedDate = $row['modified_date'];
         }
      }

      return $booking;
   }

   //update booking via id
   public function updateBooking($id, $applicationStatus, $remark)
   {
      $sql = "UPDATE booking SET application_status = :applicationStatus, remark = :remark, modified_date = NOW()
               WHERE id = :id";

      $booking = $this->getBookingById($id);
      $dbs = new DbStatus();


      if ($applicationStatus == 2) { // approving application
         if ($booking->room->roomStatus == 1) { // not available
            $dbs->updateStatus = false;
            $dbs->error = "The room is not available";
            return $dbs;
         } else if ($booking->room->roomStatus == 2) { // in maintenance
            $dbs->updateStatus = false;
            $dbs->error = "The room is in maintenance";
            return $dbs;
         }
         // room available - set it to not available
         $this->updateRoomStatusById($booking->room->id, 1);
      }
      // rejecting application or room no is available
      try {
         $stmt = $this->db->prepare($sql);
         $stmt->bindParam('applicationStatus', $applicationStatus);
         $stmt->bindParam('remark', $remark);
         $stmt->bindParam('id', $id);
         $res = $stmt->execute();

         $dbs->updateStatus = true;
         $dbs->error = 'none';
      } catch (PDOEXCEPTION $th) {
         $dbs->updateStatus = false;
         $dbs->error = $th->getMessage();
      }
      return $dbs;
   }

   ///////////////// Bookings End //////////////////////


   ///////////////// Rooms Start //////////////////////

   //  get all rooms
   public function getRooms()
   {
      $sql = "SELECT *
                    FROM room";

      $stmt = $this->db->prepare($sql);
      $stmt->execute();
      $row_count = $stmt->rowCount();

      $rooms = [];

      if ($row_count) {
         while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $room = new Room();
            $room->id = $row['id'];
            $room->roomNo = $row['room_no'];
            $room->roomType = $room->getRoomType($row['room_type']);
            $room->roomStatus = $row['room_status'];
            $room->bedArrangement = $row['bed_arrangement'];
            $room->block = $row['block'];
            $room->college = $row['college'];
            $room->lastModifiedDate = $row['modified_date'];

            array_push($rooms, $room);
         }
      }

      return $rooms;
   }

   public function getRoomById($id)
   {
      $sql = "SELECT *
                    FROM room where id = :id";

      $stmt = $this->db->prepare($sql);
      $stmt->bindParam("id", $id);
      $stmt->execute();
      $row_count = $stmt->rowCount();

      $room = new Room();

      if ($row_count == 1) {
         while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $room->id = $row['id'];
            $room->roomNo = $row['room_no'];
            $room->roomType = $room->getRoomType($row['room_type']);
            $room->roomStatus = $row['room_status'];
            $room->bedArrangement = $row['bed_arrangement'];
            $room->block = $row['block'];
            $room->college = $row['college'];
            $room->lastModifiedDate = $row['modified_date'];
         }
      }

      return $room;
   }

   public function getUserByRoomId($id)
   {
      $sql = "SELECT users.uni_id AS uniID, users.name AS name, users.email AS email, users.mobileno
              AS mobileno, users.photo AS photo,
              booking.application_status AS applicationStatus, booking.apply_date AS applyDate
              FROM room 
              JOIN booking ON room.id = booking.room
              JOIN users ON booking.user = users.id 
              where room.id = :id";

      $stmt = $this->db->prepare($sql);
      $stmt->bindParam("id", $id);
      $stmt->execute();
      $row_count = $stmt->rowCount();

      $users = [];

      if ($row_count) {
         while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $user = new Student();
            $user->uniID = $row['uni'];
            $user->name = $row['id'];
            $user->email = $row['id'];
            $user->mobileno = $row['id'];
            $user->photo = $row['id'];
            array_push($users, $user);
         }
      }
   }

   public function getRoomsCounterparts($id, $room_no, $block, $college)
   {
      $sql = "SELECT * FROM room 
              WHERE room_no =:room_no 
              AND block = :block
              AND college = :college
              AND id != :id";
      $stmt = $this->db->prepare($sql);
      $stmt->bindParam('room_no', $room_no);
      $stmt->bindParam('block', $block);
      $stmt->bindParam('college', $college);
      $stmt->bindParam('id', $id);
      $stmt->execute();
      $row_count = $stmt->rowCount();


      if ($row_count == 1) {
         $room = new Room();

         while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $room->id = $row['id'];
            $room->roomNo = $row['room_no'];
            $room->roomType = $room->getRoomType($row['room_type']);
            $room->roomStatus = $row['room_status'];
            $room->bedArrangement = $row['bed_arrangement'];
            $room->block = $row['block'];
            $room->college = $row['college'];
            $room->lastModifiedDate = $row['modified_date'];
         }

         return $room;
      } else if ($row_count > 1) {
         throw new Exception("Db Error - More than 2 room counterparts");
      }

      return null;
   }

   public function updateRoomById($id, $roomNo, $block, $college, $bedArrangement)
   {
      $sql = "UPDATE room SET 
              room_no = :room_no,
              block = :block, 
              college = :college,
              bed_arrangement = :bedArrangement,
              modified_date = NOW()
              WHERE id = :id";
      $dbs = new DbStatus();
      try {
         $stmt = $this->db->prepare($sql);
         $stmt->bindParam('room_no', $roomNo);
         $stmt->bindParam('block', $block);
         $stmt->bindParam('college', $college);
         $stmt->bindParam('bedArrangement', $bedArrangement);
         $stmt->bindParam('id', $id);
         $stmt->execute();

         $dbs->updateStatus = true;
         $dbs->error = 'none';
      } catch (PDOEXCEPTION $th) {
         $dbs->updateStatus = false;
         $dbs->error = $th->getMessage();
      }
      return $dbs;
   }

   public function updateRoomStatusById($id, $status)
   {
      $sql = "UPDATE room SET 
              room_status = :room_status, 
              modified_date = NOW()
              WHERE id = :id";
      $dbs = new DbStatus();
      try {
         $stmt = $this->db->prepare($sql);
         $stmt->bindParam('room_status', $status);
         $stmt->bindParam('id', $id);
         $stmt->execute();

         $dbs->updateStatus = true;
         $dbs->error = 'none';
      } catch (PDOEXCEPTION $th) {
         $dbs->updateStatus = false;
         $dbs->error = $th->getMessage();
      }
      return $dbs;
   }

   public function checkRoomCombination($roomNo, $block, $college, $limit)
   {
      $sql = "SELECT *
              FROM room
              WHERE room_no = :room_no AND
              block = :block AND college = :college
              ";

      try {
         $stmt = $this->db->prepare($sql);
         $stmt->bindParam('room_no', $roomNo);
         $stmt->bindParam('block', $block);
         $stmt->bindParam('college', $college);
         $stmt->execute();
         $row_count = $stmt->rowCount();
         if ($row_count > $limit)
            return false;
      } catch (PDOEXCEPTION $th) {
         return false;
      }
      return true;
   }
   /*
   // Room Creation
   public function createRoomCounterpart($room)
   {
      global $roomTypeArray;
      $sql = "INSERT INTO room (room_no, room_type, room_status, bed_arrangement, block, college, modified_date)
              VALUES (:room_no, :room_type, :room_status, 'B', :block, :college, NOW()";
      $dbs = new DbStatus();
      try {
         // Get key or index of roomType, db support number as type
         // Return false if room type not found
         $keys = array_keys($room->roomType, $roomTypeArray);
         if(is_bool($keys)) throw new PDOException("Room not in global roomTypeArray variable");

         $stmt = $this->db->prepare($sql);
         $stmt->bindParam('room_no', $room->roomNo);
         $stmt->bindParam('room_type', $keys);
         $stmt->bindParam('room_status', $room->roomStatus);
         $stmt->bindParam('block', $room->block);
         $stmt->bindParam('college', $room->college);
         $stmt->execute();
   
         $dbs->updateStatus = true;
         $dbs->error = 'none';
      } catch (PDOEXCEPTION $th) {
         $dbs->updateStatus = false;
         $dbs->error = $th->getMessage();
      }
      return $dbs;
   }

   // Room Deletion
   public function deleteRoomById($id)
   {
      $sql = "DELETE FROM room WHERE id = :id";
      $dbs = new DbStatus();

      try {
         $stmt = $this->db->prepare($sql);
         $stmt->bindParam('id', $id);

         $stmt->execute();

         $dbs->updateStatus = true;
         $dbs->error = 'none';
      } catch (PDOEXCEPTION $th) {
         $dbs->updateStatus = false;
         $dbs->error = $th->getMessage();
      }
      return $dbs;
   }

   // Update Room type, default bed arrangement => A
   public function updateRoomTypeById($id, $room_type)
   {
      $sql = "UPDATE room
              SET room_type = :room_type,
              bed_arrangement = 'A',
              WHERE id = :id";

      $dbs = new DbStatus();
      try {
         $stmt = $this->db->prepare($sql);
         $stmt->bindParam('room_type', $room_type);
         $stmt->bindParam('id', $id);

         $stmt->execute();

         $dbs->updateStatus = true;
         $dbs->error = 'none';
      } catch (PDOEXCEPTION $th) {
         $dbs->updateStatus = false;
         $dbs->error = $th->getMessage();
      }
      return $dbs;
   }
   */

   /////////////////////////////////////////////////////
   //  SECTION TO BE REARRANGED, USED BY ROOM MODULE  //
   /////////////////////////////////////////////////////
   public function getApprovedBookingsByRoomId($id)
   {
      $sql = "SELECT *
                    FROM booking WHERE room = :id AND application_status = 2";

      $stmt = $this->db->prepare($sql);
      $stmt->bindParam('id', $id);
      $stmt->execute();
      $row_count = $stmt->rowCount();

      $bookings = [];

      if ($row_count) {
         while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $booking = new Booking();
            $booking->id = $row['id'];
            $booking->room = $row['room'];
            $booking->user = $row['user'];
            $booking->applicationStatus = $row['application_status'];
            $booking->remark = $row['remark'];
            $booking->applyDate = $row['apply_date'];
            $booking->lastModifiedDate = $row['modified_date'];

            array_push($bookings, $booking);
         }
      }

      return $bookings;
   }

   public function getStudentById($id)
   {
      $sql = "SELECT * FROM users WHERE id = :id";
      $stmt = $this->db->prepare($sql);
      $stmt->bindParam('id', $id);
      $stmt->execute();
      $row_count = $stmt->rowCount();

      if ($row_count == 1) {
         $user = new Student();

         while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $user->id = $row['id'];
            $user->uniID = $row['uni_id'];
            $user->username = $row['username'];
            $user->passwordhash = $row['password'];
            $user->name = $row['name'];
            $user->email = $row['email'];
            $user->mobileno = $row['mobileno'];
            $user->photo = $row['photo'];
            $user->userType = $row['user_type'];
            $user->addedDate = $row['addedDate'];
         }

         return $user;
      } else if ($row_count > 1) {
         throw new Exception("Db Error - Multiple user with same id");
      }
   }

   //add new application 
   public function addBooking($roomID, $userID)
   {
      try {

         $sql = "INSERT INTO booking(room, user, application_status, apply_date) 
                    VALUES (:roomID, :userID, :application_status, NOW())";
         $stmt = $this->db->prepare($sql);
         $stmt->bindValue("roomID", (int) $roomID);
         $stmt->bindValue("userID", $userID);
         $stmt->bindValue("application_status", "Pending");
         $stmt->execute();

         $dbs = new DbStatus();
         $dbs->status = true;
         $dbs->error = "none";
         $dbs->lastinsertid = $this->db->lastInsertId();

         return $dbs;
      } catch (PDOException $e) {
         $errorMessage = $e->getMessage();

         $dbs = new DbStatus();
         $dbs->status = false;
         $dbs->error = $errorMessage;

         return $dbs;
      }
   }

   //add new application end

   //check appplication duplicated start
   public function checkApplication($userID)
   {
      $sql = "SELECT *
      FROM booking 
      WHERE user = :user";
      $db = getDatabase();

      $stmt = $this->db->prepare($sql);
      $stmt->bindParam("user", $userID);
      $stmt->execute();
      $row_count = $stmt->rowCount();

      if ($row_count > 0) {
         while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            if ($row['application_status'] == 0 || $row['application_status'] == 2) {
               $dbs = new DbStatus();
               $dbs->status = true;
               $dbs->error = "none";
               break;
            } else {
               $dbs = new DbStatus();
               $dbs->status = false;
               $dbs->error = "No application";
            }
         }
      } else {
         //$errorMessage = $e->getMessage();

         $dbs = new DbStatus();
         $dbs->status = false;
         $dbs->error = "No application";
      }

      return $dbs;
   }
   ///get application
   public function getApplicationList($userID)
   {
      $sql = "SELECT *
      FROM booking 
      WHERE user = :user";
      $db = getDatabase();

      $stmt = $this->db->prepare($sql);
      $stmt->bindParam("user", $userID);
      $stmt->execute();
      $row_count = $stmt->rowCount();
      $data = array();

      if ($row_count) {
         while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            $booking = new Booking();
            $booking->id = $row['id'];
            $booking->room = $row['room'];
            $booking->applicationStatus = $row['application_status'];
            $booking->remark = $row['remark'];
            $booking->applyDate = $row['apply_date'];
            $booking->lastModifiedDate = $row['modified_date'];
            $booking->roomdb = $this->getRoomById($row['room']);


            array_push($data, $booking);
         }
      }

      return $data;
   }
   /////////////////////////////////////////////////////
   //  SECTION TO BE REARRANGED, USED BY ROOM MODULE  //
   /////////////////////////////////////////////////////
   //  rooms end


}
