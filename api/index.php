<?php

ini_set("date.timezone", "Asia/Kuala_Lumpur");

header('Access-Control-Allow-Origin: *');

//*
// Allow from any origin
if (isset($_SERVER['HTTP_ORIGIN'])) {
   // Decide if the origin in $_SERVER['HTTP_ORIGIN'] is one
   // you want to allow, and if so:
   header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
   header('Access-Control-Allow-Credentials: true');
   header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

   if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
      header("Access-Control-Allow-Methods: GET, POST, DELETE, PUT, OPTIONS");

   if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
      header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

   exit(0);
}
//*/

include_once("database_class.php");

require_once 'vendor/autoload.php';

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;

//load environment variable - jwt secret key
$dotenv = new Dotenv\Dotenv(__DIR__);
$dotenv->load();

//jwt secret key in case dotenv not working in apache
//$jwtSecretKey = "jwt_secret_key";

use Slim\App;
use Slim\Middleware\TokenAuthentication;
use Firebase\JWT\JWT;

//functions /////////////////////////////////////////////start

function generateToken($role, $login, $name, $userid)
{

   //create JWT token
   $date = date_create();
   $jwtIAT = date_timestamp_get($date);
   $jwtExp = $jwtIAT + (180 * 60); //expire after 3 hours

   $jwtToken = array(
      "iss" => "contacts.net", //issuer of the token
      "iat" => $jwtIAT, //issued at time
      "exp" => $jwtExp, //expire
      "role" => $role,
      "login" => $login,
      "name" => $name,
      "userid" => $userid
   );
   $token = JWT::encode($jwtToken, getenv('JWT_SECRET'));
   return $token;
}

function getDatabase()
{
   $dbhost = "localhost";
   $dbuser = "root";
   $dbpass = "";
   $dbname = "college_registration";

   $db = new Database($dbhost, $dbuser, $dbpass, $dbname);
   return $db;
}

function getDecodedTokenFromTokenPayload($request, $response)
{
   $token_array = $request->getHeader('HTTP_AUTHORIZATION');
   $token = substr($token_array[0], 7);

   //decode the token
   try {
      $tokenDecoded = JWT::decode(
         $token,
         getenv('JWT_SECRET'),
         array('HS256')
      );

      //in case dotenv not working
      /*
         $tokenDecoded = JWT::decode(
            $token, 
            $GLOBALS['jwtSecretKey'], 
            array('HS256')
         );
         */
   } catch (Exception $e) {
      $data = array(
         "message" => "Token invalid"
      );

      return $response->withJson($data, 401)
         ->withHeader('Content-type', 'application/json');
   }

   return $tokenDecoded;
}

function getLoginFromTokenPayload($request, $response)
{
   $tokenDecoded = getDecodedTokenFromTokenPayload($request, $response);
   return $tokenDecoded->login;
}

function getIdFromTokenPayload($request, $response)
{
   $tokenDecoded = getDecodedTokenFromTokenPayload($request, $response);
   return $tokenDecoded->userid;
}

function getRoleFromTokenPayload($request, $response)
{
   $tokenDecoded = getDecodedTokenFromTokenPayload($request, $response);
   return strtolower($tokenDecoded->role);
}
//functions /////////////////////////////////////////////ends

$config = [
   'settings' => [
      'displayErrorDetails' => true
   ]
];

$app = new App($config);

/**
 * Token authentication middleware logic
 */
$authenticator = function ($request, TokenAuthentication $tokenAuth) {

   /**
    * Try find authorization token via header, parameters, cookie or attribute
    * If token not found, return response with status 401 (unauthorized)
    */
   $token = $tokenAuth->findToken($request); //from header

   try {
      $tokenDecoded = JWT::decode($token, getenv('JWT_SECRET'), array('HS256'));

      //in case dotenv not working
      //$tokenDecoded = JWT::decode($token, $GLOBALS['jwtSecretKey'], array('HS256'));
   } catch (Exception $e) {
      throw new \app\UnauthorizedException('Invalid Token');
   }
};

/**
 * Add and manage token authentication middleware => $authenticator
 * passthrough means, no token needed, a public/guest route
 */
$app->add(new TokenAuthentication([
   'path' => '/', //secure route - need token
   'passthrough' => [ //public route, no token needed
      '/ping',
      '/token',
      '/auth',
      '/hello',
      '/calc',
      '/registration',
      '/viewapplication'
   ],
   'authenticator' => $authenticator
]));

/**
 * Public route example
 */
$app->get('/ping', function ($request, $response) {
   $output = ['msg' => 'RESTful API works, active and online!'];
   return $response->withJson($output, 200, JSON_PRETTY_PRINT);
});

/**
 * Public route example
 * in real application, this must be disable
 * only for debugging, generating token instantly
 * without login
 */
// $app->get('/token', function ($request, $response) {
//    $token = generateToken('member', 'bean', 'mr bean');
//    $output = ['token' => $token];
//    return $response->withJson($output, 200, JSON_PRETTY_PRINT);
// });


/**
 * Public route /auth for creds authentication / login process
 */
$app->post('/auth', function ($request, $response) {
   //extract form data - username and password
   $json = json_decode($request->getBody());
   $username = $json->username;
   $clearpassword = $json->password;

   //do db authentication
   $db = getDatabase();
   $data = $db->authenticateUser($username);
   $db->close();

   //status -1 -> user not found
   //status 0 -> wrong password
   //status 1 -> login success

   $returndata = array();

   //user not found
   if ($data === NULL) {
      $returndata = array(
         "loginStatus" => false,
         "errorMessage" => "Username/password is incorrect!"
      );
   } else { //user found

      if (password_verify($clearpassword, $data->passwordhash)) {

         //create JWT token
         $date = date_create();
         $jwtIAT = date_timestamp_get($date);
         $jwtExp = $jwtIAT + (60 * 60 * 12); //expire after 12 hours

         $jwtToken = array(
            // "iss" => "mycontacts.net", //token issuer
            "iss" => "mycollege.net", //token issuer
            "iat" => $jwtIAT, //issued at time
            "exp" => $jwtExp, //expire
            "role" => $data->userType,
            "login" => $data->username,
            "name" => $data->name,
            "userid" => $data->userid
         );
         $token = JWT::encode($jwtToken, getenv('JWT_SECRET'));

         $returndata = array(
            "loginStatus" => true,
            "token" => $token,
         );
      } else {

         $returndata = array(
            "loginStatus" => false,
            "errorMessage" => "Username/password is incorrect!"
         );
      }
   }

   return $response->withJson($returndata, 200)
      ->withHeader('Content-type', 'application/json');
});

//restricted route
//refresh token
//if current token valid, extend token for another 15 minutes
$app->post('/refreshtoken', function ($request, $response) {

   $token_array = $request->getHeader('HTTP_AUTHORIZATION');
   $token = substr($token_array[0], 7);

   $decodedToken = new stdClass();
   $isValidToken = false;

   //we need to validate the token, for decoding it, no choice
   //double validation here
   //this is restricted route, so token validation happen in middleware
   try {
      $decodedToken = JWT::decode($token, getenv('JWT_SECRET'), array('HS256'));
   } catch (Exception $e) {
      $data = array(
         "message" => "Invalid Token"
      );

      return $response->withJson($data, 401)
         ->withHeader('Content-type', 'application/json');
   }

   $role = $decodedToken->role;
   $login = $decodedToken->login;
   $name = $decodedToken->name;
   $userid = $decodedToken->userid;

   $token = generateToken($role, $login, $name, $userid);

   $data = array(
      "token" => $token,
      "isValidToken" => true
   );

   return $response->withJson($data, 200)
      ->withHeader('Content-tye', 'application/json');
});


//users CRUD  //////////////////////////////////////////////////////////////////////  starts

//restricted route
//- secure route - need token
//GET - students list using login in token payload
$app->get('/users/students', function ($request, $response) {

   $username = getLoginFromTokenPayload($request, $response);

   $db = getDatabase();
   $data = $db->getStudents();
   $db->close();

   return $response->withJson($data, 200)
      ->withHeader('Content-type', 'application/json');
});

//restricted route
//- secure route - need token
//GET - single user using login in token payload
$app->get('/users', function ($request, $response) {

   $username = getLoginFromTokenPayload($request, $response);

   $db = getDatabase();
   $data = $db->getUserViaLogin($username);
   $db->close();

   return $response->withJson($data, 200)
      ->withHeader('Content-type', 'application/json');
});

//restricted route
//- secure route - need token
//PUT - UPDATE SINGLE user VIA login from token payload
$app->put('/users', function ($request, $response, $args) {

   $username = getLoginFromTokenPayload($request, $response);

   //form data using json structure
   $json = json_decode($request->getBody());
   $name = $json->name;
   $email = $json->email;
   $mobileno = $json->mobileno;

   $db = getDatabase();
   $dbs = $db->updateUserViaLogin($username, $name, $email, $mobileno);
   $db->close();

   $data = array(
      "updateStatus" => $dbs->status,
      "errorMessage" => $dbs->error
   );

   return $response->withJson($data, 200)
      ->withHeader('Content-type', 'application/json');
});

//restricted route
//- secure route - need token
//PUT - UPDATE SINGLE user VIA login from token payload
$app->put('/users/resetpassword', function ($request, $response, $args) {

   $username = getLoginFromTokenPayload($request, $response);

   //form data using json structure
   $json = json_decode($request->getBody());
   $oldpassword = $json->oldpassword;
   $clearpassword = $json->newpassword;

   $db = getDatabase();

   $passwordhash = $db->getUserPasswordViaLogin($username);

   //check the hash against the oldpassword
   if (password_verify($oldpassword, $passwordhash)) {

      //same, proceed for password reset/update         

      //update users table for new password
      $dbs = $db->updateUserPasswordViaLogin($username, $clearpassword);

      if ($dbs->status)
         $data = array(
            "updateStatus" => true
         );
      else
         $data = array(
            "updateStatus" => false,
            "errorMessage" => $dbs->error
         );

      $db->close();

      return $response->withJson($data, 200)
         ->withHeader('Content-type', 'application/json');
   } else { //oldpassword not the same as the one in db

      $data = array(
         "updateStatus" => false,
         "errorMessage" => "Old password is incorrect!"
      );

      $db->close();

      return $response->withJson($data, 200)
         ->withHeader('Content-type', 'application/json');
   }
});

//restricted route
//- secure route - need token
//POST - UPDATE SINGLE user Profile Photo VIA login from token payload
$app->post('/users/uploadprofilepic', function ($request, $response, $args) {

   $id = $_POST["id"];

   $target_dir = "../img/";
   $uniqid = uniqid();
   $filename = $id . "_" . $uniqid . "_" . basename($_FILES["fileToUpload"]["name"]);
   $target_file = $target_dir . $filename;

   $uploadStatus = false;

   if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
      $uploadStatus = true;

      //save the image name to db here
      try {
         $db = getDatabase();
         $dbs = $db->updateProfilePhoto($filename, $id);
      } catch (PDOException $e) {
         $errorMessage = $e->getMessage();
         $data = array(
            "uploadstatus" => false,
            "errorMessage" => $errorMessage
         );
         echo json_encode($data);
         exit;
      }
   } else {
      $uploadStatus = false;
   }

   if ($uploadStatus) {
      $info = array(
         "uploadstatus" => $uploadStatus,
         "photofilename" => $filename,
      );
   } else {
      $info = array(
         "uploadstatus" => $uploadStatus,
      );
   }
   return $response->withJson($info, 200)
      ->withHeader('Content-type', 'application/json');
});


//Public route /registration for member registration
$app->post('/registration', function ($request, $response) {

   $json = json_decode($request->getBody());
   $username = $json->username;
   $clearpassword = $json->password;
   $name = $json->name;
   $matric = $json->matric;
   $email = $json->email;
   $mobile = $json->mobile;

   //insert user
   $db = getDatabase();
   $dbs = $db->insertUser($username, $clearpassword, $name, $matric, $email, $mobile);
   $db->close();

   $data = array(
      "registrationStatus" => $dbs->status,
      "errorMessage" => $dbs->error
   );

   return $response->withJson($data, 200)
      ->withHeader('Content-type', 'application/json');
});

//check application duplicated
$app->get('/users/{id}/bookings/duplicated', function ($request, $response, $args) {
   $userid = intval($args['id']);

   $db = getDatabase();
   $dbs = $db->checkApplication($userid);
   $db->close();

   $data = array(
      "applicationStatus" => $dbs->status,
      "errorMessage" => $dbs->error
   );

   return $response->withJson($data, 200)
      ->withHeader('Content-type', 'application/json');
});

$app->get('/users/{id}/bookings', function ($request, $response, $args) {
   $userid = intval($args['id']);
   $db = getDatabase();
   $data = $db->getApplicationList($userid);
   $db->close();

   return $response->withJson($data, 200)->withHeader('Content-type', 'application/json');
});

//users CRUD  ////////////////////////////////////////////////////////////////////////  ends

//Booking CRUD  //////////////////////////////////////////////////////////////////////  starts
$app->get("/bookings", function ($request, $response, $args) {
   if (getRoleFromTokenPayload($request, $response) == 'staff') {
      $db = getDatabase();
      $res = $db->getBookings();
      return $response->withJson($res, 200)->withHeader('Content-type', 'application/json');
   } else {
      $data = array(
         "message" => "Unauthorized Access. Login as an admin to continue."
      );

      return $response->withJson($data, 401)
         ->withHeader('Content-type', 'application/json');
   }
});

$app->get("/bookings/[{id}]", function ($request, $response, $args) {
   $db = getDatabase();
   $res = $db->getBookingById($args['id']);

   if (getRoleFromTokenPayload($request, $response) == 'staff' || $res->user->id == getIdFromTokenPayload($request, $response)) {
      return $response->withJson($res, 200)->withHeader('Content-type', 'application/json');
   } else {
      $data = array(
         "message" => "Unauthorized Access"
      );

      return $response->withJson($data, 401)
         ->withHeader('Content-type', 'application/json');
   }
});

$app->put("/bookings/[{id}]", function ($request, $response, $args) {
   if (getRoleFromTokenPayload($request, $response) == 'staff') {
      $db = getDatabase();
      $data = json_decode($request->getBody());
      $res = $db->updateBooking($args['id'], $data->applicationStatus, $data->remark);
      return $response->withJson($res, 200)->withHeader('Content-type', 'application/json');
   } else {
      $data = array(
         "message" => "Unauthorized Access. Login as an admin to continue."
      );

      return $response->withJson($data, 401)
         ->withHeader('Content-type', 'application/json');
   }
});

$app->post('/bookings/[{id}]', function ($request, $response, $args) {
   $roomID = intval($args['id']);

   $token_array = $request->getHeader('HTTP_AUTHORIZATION');
   $token = substr($token_array[0], 7);

   //decode the token get user id
   try {
      $tokenDecoded = JWT::decode(
         $token,
         getenv('JWT_SECRET'),
         array('HS256')
      );
      $userID = $tokenDecoded->userid;
   } catch (Exception $e) {
      $data = array(
         "message" => "Token invalid"
      );

      return $response->withJson($data, 401)
         ->withHeader('Content-tye', 'application/json');
   }



   $db = getDatabase();
   $dbs = $db->addBooking($roomID, $userID);
   $db->close();

   $data = array(
      "bookingStatus" => $dbs->status,
      "errorMessage" => $dbs->error
   );

   return $response->withJson($data, 200)
      ->withHeader('Content-type', 'application/json');
});

// $app->get('/application/view/[{user}]/[{room}]', function ($request, $response, $args) {


//    $userid = intval($args['user']);
//    $roomid = intval($args['room']);
//    $db = getDatabase();
//    $data = $db->getApplication($userid,$roomid);
//    $db->close();

//    return $response->withJson($data, 200)->withHeader('Content-type', 'application/json');
// });

//Booking CRUD  //////////////////////////////////////////////////////////////////////  ends






//Room CRUD  //////////////////////////////////////////////////////////////////////  starts
$app->get("/rooms", function ($request, $response, $args) {
   $db = getDatabase();
   $res = $db->getRooms();
   return $response->withJson($res, 200)->withHeader('Content-type', 'application/json');
});

$app->get("/rooms/[{id}]", function ($request, $response, $args) {
   $id = $args['id'];

   $db = getDatabase();
   try {
      $room = $db->getRoomById($id);

      $bookings = $db->getApprovedBookingsByRoomId($id);

      $users = [];
      foreach ($bookings as $booking) {
         $user = $db->getStudentById($booking->user);
         $arr = array('booking' => $booking, 'user' => $user);
         array_push($users, $arr);
      }
      $res = array('room' => $room, 'users' => $users);
   } catch (Exception $err) {
      $error = array('error' => $err->getMessage());
      return $response->withJson($error, 500)->withHeader('Content-type', 'application/json');
   }

   return $response->withJson($res, 200)->withHeader('Content-type', 'application/json');
});

$app->get("/rooms/edit/[{id}]", function ($request, $response, $args) {
   $id = $args['id'];

   $db = getDatabase();
   try {
      $res = $db->getRoomById($id);
   } catch (Exception $err) {
      $error = array('error' => $err->getMessage());
      return $response->withJson($error, 500)->withHeader('Content-type', 'application/json');
   }

   return $response->withJson($res, 200)->withHeader('Content-type', 'application/json');
});

$app->put("/rooms/[{id}]", function ($request, $response, $args) {
   $id = $args['id'];

   $json = json_decode($request->getBody());

   $toUpdateBlock = $json->block;
   $toUpdateCollege = $json->college;
   $toUpdateRoomNo = $json->roomNo;
   $toUpdateBedArrangement = $json->bedArrangement;
   $toUpdateRoomStatus = $json->roomStatus;

   if (
      $id == NULL || $toUpdateBlock == NULL || $toUpdateCollege == NULL ||
      $toUpdateRoomNo == NULL || $toUpdateBedArrangement == NULL || $toUpdateRoomStatus == NULL
   ) {
      return $response->withStatus(500)
         ->withHeader('Content-type', 'application/json')
         ->write(json_encode(array(
            'error' => 'Form Incomplete Exception',
            'error_message' => "Please dont leave the fields empty",
            'status_code' => 500,
         )));
   }
   $dbs = new DbStatus();

   $db = getDatabase();
   try {
      $room = $db->getRoomById($id);
      $currBedArrangement = $room->bedArrangement;
      if ($room->roomType == 'Double') {
         $counterpart = $db->getRoomsCounterparts($id, $room->roomNo, $room->block, $room->college);
         // if counterpart does not exist
         if ($counterpart == NULL) {
            throw new Exception('Double Room but no adjacent bed found, possible room creation error');
         } else {
            $counterpartBedArrangement = $counterpart->bedArrangement;

            if ($counterpartBedArrangement == $currBedArrangement) {
               throw new Exception('Room with two same bed arrangement');
            } else if ($currBedArrangement != $toUpdateBedArrangement) {
               if ($counterpartBedArrangement != $toUpdateBedArrangement) {
                  throw new Exception('Tampered input or messed up bed arrangement of same room');
               }
               // Swap bed arrangements
               $tmp = $currBedArrangement;
               $currBedArrangement = $counterpartBedArrangement;
               $counterpartBedArrangement = $tmp;
            }

            // Begin Transaction
            $stat = $db->beginTransaction();
            if ($stat != 1) throw new Exception($stat);

            //update counterpart
            $db->updateRoomById($counterpart->id, $toUpdateRoomNo, $toUpdateBlock, $toUpdateCollege, $counterpartBedArrangement);
            //update current room
            $db->updateRoomById($id, $toUpdateRoomNo, $toUpdateBlock, $toUpdateCollege, $currBedArrangement);
            $db->updateRoomStatusById($id, $toUpdateRoomStatus);

            $updateOk = $db->checkRoomCombination($toUpdateRoomNo, $toUpdateBlock, $toUpdateCollege, 2);
            if (!$updateOk) {
               $stat = $db->rollback();
               if ($stat != 1) throw new Exception($stat);
               throw new Exception('Duplicated combination of roomNo, block and college when trying to update the room details');
            };

            // Commit Transaction
            $stat = $db->commit();
            if ($stat != 1) throw new Exception($stat);
         }
      } else { // Not double room
         $counterpart = $db->getRoomsCounterparts($id, $room->roomNo, $room->block, $room->college);
         if ($counterpart != NULL)
            throw new Exception('One or more room with duplicated room_no, college, block combination found');

         if ($currBedArrangement != $toUpdateBedArrangement) // Unmatch bedArrangement for updating single room
            throw new Exception('Tampered bed arrangement input field or single room with non A bed');

         // Begin Transaction
         $stat = $db->beginTransaction();
         if ($stat == 0) throw new Exception("Transaction Error");

         $db->updateRoomById($id, $toUpdateRoomNo, $toUpdateBlock, $toUpdateCollege, $currBedArrangement);
         $db->updateRoomStatusById($id, $toUpdateRoomStatus);


         $updateOk = $db->checkRoomCombination($toUpdateRoomNo, $toUpdateBlock, $toUpdateCollege, 1);
         if (!$updateOk) {
            $stat = $db->rollback();
            if ($stat != 1) throw new Exception($stat);
            throw new Exception('Duplicated combination of roomNo, block and college when trying to update the room details');
         };

         // Commit Transaction
         $stat = $db->commit();
         if ($stat == 0) throw new Exception("Commit Error");
      }
      $db->close();
      $dbs->updateStatus = true;
      $dbs->error = 'none';
   } catch (Exception $err) {
      return $response->withStatus(500)
         ->withHeader('Content-type', 'application/json')
         ->write(json_encode(array(
            'error' => 'DB Operation Exception',
            'error_message' => $err->getMessage(),
            'status_code' => 500,
         )));
   }

   return $response->withJson($dbs, 200)->withHeader('Content-type', 'application/json');
});

//Room CRUD  //////////////////////////////////////////////////////////////////////  ends

$app->run();
