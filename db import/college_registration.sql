-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 15, 2020 at 01:43 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.3.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `college_registration`
--
CREATE DATABASE IF NOT EXISTS `college_registration` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `college_registration`;

-- --------------------------------------------------------

--
-- Table structure for table `booking`
--

CREATE TABLE `booking` (
  `id` int(11) NOT NULL,
  `room` int(11) NOT NULL,
  `user` int(11) NOT NULL,
  `application_status` int(11) NOT NULL,
  `remark` text NOT NULL,
  `apply_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `booking`
--

INSERT INTO `booking` (`id`, `room`, `user`, `application_status`, `remark`, `apply_date`, `modified_date`) VALUES
(13, 2, 2, 1, 'This is for international student only.', '2020-07-15 19:37:04', '2020-07-15 19:38:04'),
(14, 4, 3, 1, 'Priority given to international student', '2020-07-15 19:37:35', '2020-07-15 19:39:40'),
(15, 1, 2, 2, 'No problem', '2020-07-15 19:38:42', '2020-07-15 19:40:39'),
(16, 6, 3, 0, '', '2020-07-15 19:40:14', '2020-07-15 19:40:14'),
(17, 2, 4, 0, '', '2020-07-15 19:41:39', '2020-07-15 19:41:39');

-- --------------------------------------------------------

--
-- Table structure for table `room`
--

CREATE TABLE `room` (
  `id` int(11) NOT NULL,
  `room_no` int(10) NOT NULL,
  `room_type` tinyint(4) NOT NULL,
  `room_status` tinyint(4) NOT NULL,
  `bed_arrangement` char(5) NOT NULL,
  `block` varchar(10) NOT NULL,
  `college` varchar(150) NOT NULL,
  `modified_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `room`
--

INSERT INTO `room` (`id`, `room_no`, `room_type`, `room_status`, `bed_arrangement`, `block`, `college`, `modified_date`) VALUES
(1, 111, 0, 1, 'A', 'MA1', 'KTDI', '2020-07-15 19:40:39'),
(2, 237, 1, 0, 'A', 'MA1', 'KTDI', '2020-07-03 18:17:44'),
(3, 237, 1, 0, 'B', 'MA1', 'KTDI', '2020-07-03 18:17:44'),
(4, 312, 0, 0, 'A', 'M20', 'KTDI', '2020-07-03 18:17:44'),
(5, 313, 0, 0, 'A', 'M20', 'KTDI', '2020-07-03 18:17:44'),
(6, 101, 1, 0, 'A', 'G20', 'KTR', '2020-07-03 18:17:44'),
(7, 101, 1, 0, 'B', 'G20', 'KTR', '2020-07-03 18:17:44');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `uni_id` varchar(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(250) NOT NULL,
  `name` varchar(150) NOT NULL,
  `email` varchar(250) NOT NULL,
  `mobileno` varchar(15) NOT NULL,
  `photo` varchar(150) NOT NULL,
  `user_type` enum('Staff','Student','') NOT NULL,
  `addedDate` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `uni_id`, `username`, `password`, `name`, `email`, `mobileno`, `photo`, `user_type`, `addedDate`) VALUES
(1, '1', 'admin1', '$2y$10$avpJWG36A35JVL6zBnWQqeRq1CRsBxayITYgwZnnvXZsHVNzm2kKq', 'Johnson Kenny', 'johnson@example.com', '0123123123', '', 'Staff', '2020-07-03 18:14:14'),
(2, '2', 'student1', '$2y$10$DvvwScMJ.gBW2tvqyb0LOOPdGM8rBnE.wzN7bqrdXGJgQyyvBab5q', 'Tomothy Garcia', 'tomothy.garcia@example.com', '2402372923', '', 'Student', '2020-07-03 18:50:28'),
(3, '3', 'student2', '$2y$10$DvvwScMJ.gBW2tvqyb0LOOPdGM8rBnE.wzN7bqrdXGJgQyyvBab5q', 'Melvin Murphy', 'melvin.murphy@example.com', '2577477891', '', 'Student', '2020-07-03 18:50:28'),
(4, '4', 'student3', '$2y$10$DvvwScMJ.gBW2tvqyb0LOOPdGM8rBnE.wzN7bqrdXGJgQyyvBab5q', 'Erik Horton', 'Erik.horton@example.com', '8862283866', '', 'Student', '2020-07-03 18:50:28'),
(5, '5', 'student4', '$2y$10$DvvwScMJ.gBW2tvqyb0LOOPdGM8rBnE.wzN7bqrdXGJgQyyvBab5q', 'Marian Webb', 'marian.webb@example.com', '4415772295', '', 'Student', '2020-07-03 18:50:28'),
(6, '6', 'student5', '$2y$10$DvvwScMJ.gBW2tvqyb0LOOPdGM8rBnE.wzN7bqrdXGJgQyyvBab5q', 'Janet Richards', 'janet.richards@example.com', '5281700454', '', 'Student', '2020-07-03 18:50:28'),
(7, '7', 'student6', '$2y$10$DvvwScMJ.gBW2tvqyb0LOOPdGM8rBnE.wzN7bqrdXGJgQyyvBab5q', 'Alyssa Daniels', 'kianseng@example.com', '0123456789', '', 'Student', '2020-07-03 18:50:28');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `booking`
--
ALTER TABLE `booking`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `room`
--
ALTER TABLE `room`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uni_id` (`uni_id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `booking`
--
ALTER TABLE `booking`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `room`
--
ALTER TABLE `room`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
